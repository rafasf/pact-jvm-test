# Pact JVM (test)

Attempt to create a contract from a consumer and have the producer verify it.
There are different ways to do so, where the two that seems interesting are:

* [x] using build plugin
* [ ] using junit


## Testing

To verify the contract between `Dummy` and `Cruncher` run:
```
$ ./gradlew :verifyContracts
```

To see the result of `Dummy` consuming information from `Cruncher` run:
```
$ ./gradlew seeSummary
```

## Documentation

Classes have notes on them trying to explain the reason of their existence and a little of the its responsibility.


## Stories :)

* [x] Create a contract
* [x] Verify the contract (hard-coded)
* [ ] Use Pact broker
