package com.blah.cruncher.api;

import com.blah.cruncher.api.representation.GroupSummaryRepresentation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

import static org.springframework.http.ResponseEntity.ok;

/**
 * The {@code GroupSummariesResource} exposes summaries for groups
 * The user should be able to:
 *  * See all available summaries
 *  * Request a specific summary
 */
@RestController
@RequestMapping(value = "/groups", produces = "application/json")
public class GroupSummariesResource {
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<GroupSummaryRepresentation> groupSummaryFor(@PathVariable("id") String id) {
    GroupSummaryRepresentation groupSummary = new GroupSummaryRepresentation(
      "bau",
      "blah",
      new BigDecimal(10));

    return ok(groupSummary);
  }
}
