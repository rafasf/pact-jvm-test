package com.blah.cruncher.api.representation;

import lombok.Value;

import java.math.BigDecimal;

/**
 * The information being returned to consumers
 */
@Value
public class GroupSummaryRepresentation {
  String name;
  String field3;
  BigDecimal total;
}
