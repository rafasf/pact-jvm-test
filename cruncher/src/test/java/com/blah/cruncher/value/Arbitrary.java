package com.blah.cruncher.value;

import java.util.UUID;

public class Arbitrary {
  public static String uuidString() {
    return uuid().toString();
  }

  public static UUID uuid() {
    return UUID.randomUUID();
  }
}
