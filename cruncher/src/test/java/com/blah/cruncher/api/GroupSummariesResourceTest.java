package com.blah.cruncher.api;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.blah.cruncher.value.Arbitrary.uuidString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GroupSummariesResourceTest {
  private MockMvc mockMvc;

  @Before
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders
      .standaloneSetup(new GroupSummariesResource())
      .build();
  }

  @Test
  public void returnsTheDesiredGroupSummary() throws Exception {
    mockMvc
      .perform(get("/groups/{id}", uuidString()))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.name").value("bau"));
  }
}
