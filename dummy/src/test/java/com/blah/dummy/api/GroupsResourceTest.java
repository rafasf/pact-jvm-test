package com.blah.dummy.api;

import com.blah.dummy.domain.GroupId;
import com.blah.dummy.domain.SomeSummary;
import com.blah.dummy.gateway.cruncher.CruncherGateway;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.blah.dummy.domain.GroupId.groupId;
import static com.blah.dummy.value.Arbitrary.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GroupsResourceTest {
  @Mock CruncherGateway cruncher;

  private MockMvc mockMvc;

  @Before
  public void setUp() throws Exception {
    initMocks(this);

    mockMvc = MockMvcBuilders
      .standaloneSetup(new GroupsResource(cruncher))
      .build();
  }

  @Test
  public void returnsGroupInformation() throws Exception {
    GroupId groupId = groupId(uuidString());
    SomeSummary summary = new SomeSummary(string(), bigDecimal());

    given(cruncher.someSummaryFor(eq(groupId))).willReturn(summary);

    mockMvc
      .perform(get("/groups/{id}", groupId.value()))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.name").value("Super"))
      .andExpect(jsonPath("$.summary.name").value(summary.getName()))
      .andExpect(jsonPath("$.summary.total").value(summary.getTotal()));
  }
}
