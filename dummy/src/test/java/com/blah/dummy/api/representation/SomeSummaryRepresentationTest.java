package com.blah.dummy.api.representation;

import com.blah.dummy.domain.SomeSummary;
import com.blah.dummy.value.Arbitrary;
import org.junit.Test;

import static com.blah.dummy.api.representation.SomeSummaryRepresentation.from;
import static com.blah.dummy.value.Arbitrary.bigDecimal;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class SomeSummaryRepresentationTest {
  @Test
  public void createsRepresentationFromSomeSummary() throws Exception {
    SomeSummary summary = new SomeSummary(Arbitrary.string(), bigDecimal());

    assertThat(from(summary))
      .isEqualTo(new SomeSummaryRepresentation(summary.getName(), summary.getTotal()));
  }
}
