package com.blah.dummy.gateway.cruncher;

import au.com.dius.pact.consumer.ConsumerPactTestMk2;
import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import com.blah.dummy.domain.SomeSummary;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;

import static com.blah.dummy.domain.GroupId.groupId;
import static org.assertj.core.api.Assertions.assertThat;

public class CruncherContractTest extends ConsumerPactTestMk2 {
  @Override
  protected RequestResponsePact createPact(PactDslWithProvider builder) {
    return builder
      .uponReceiving("request for some summary")
        .path("/groups/503c448a-8b8a-440f-80b5-14d1c5965a28")
        .method("GET")
      .willRespondWith()
        .status(HttpStatus.OK.value())
        .headers(Collections.singletonMap("Content-Type", "application/json"))
        .body("{\"name\": \"bau\", \"field3\": \"blah\", \"total\": 10}")
      .toPact();
  }

  @Override
  protected String providerName() {
    return "cruncher";
  }

  @Override
  protected String consumerName() {
    return "dummy";
  }

  @Override
  protected void runTest(MockServer mockServer) throws IOException {
    CruncherGateway cruncher = new CruncherGateway(new RestTemplate(), mockServer.getUrl());
    assertThat(
      cruncher.someSummaryFor(groupId("503c448a-8b8a-440f-80b5-14d1c5965a28"))
    ).isEqualTo(new SomeSummary("bau", new BigDecimal(10)));
  }
}
