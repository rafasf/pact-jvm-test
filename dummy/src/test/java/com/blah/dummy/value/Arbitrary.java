package com.blah.dummy.value;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.math.BigDecimal;
import java.util.UUID;

public class Arbitrary {
  public static String string() {
    return RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(20, 100));
  }

  public static BigDecimal bigDecimal() {
    return new BigDecimal(RandomUtils.nextInt(20, Integer.MAX_VALUE));
  }

  public static String uuidString() {
    return uuid().toString();
  }

  public static UUID uuid() {
    return UUID.randomUUID();
  }
}
