package com.blah.dummy.domain;

import lombok.EqualsAndHashCode;

/**
 * The {@code GroupId} is a type of TinyType representing the concept of
 * a GroupId
 *
 * Under the covers it gets converted to a string. But using the types, helps
 * in keeping the code more understandable and maintainable.
 */
@EqualsAndHashCode
public class GroupId {
  private final String value;

  private GroupId(String value) {
    this.value = value;
  }

  public static GroupId groupId(String value) {
    return new GroupId(value);
  }

  public String value() {
    return this.value;
  }

  public String toString() {
    return "GroupId(value=" + this.value() + ")";
  }
}
