package com.blah.dummy.domain;

import lombok.Value;

import java.math.BigDecimal;

/**
 * The domain model which is a core concept for the "Dummy" component
 *
 * It will hold the information and logic related to it.
 */
@Value
public class SomeSummary {
  String name;
  BigDecimal total;
}
