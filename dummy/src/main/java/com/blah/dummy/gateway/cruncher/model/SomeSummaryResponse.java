package com.blah.dummy.gateway.cruncher.model;

import com.blah.dummy.domain.SomeSummary;
import lombok.Value;

import java.math.BigDecimal;

/**
 * This is a representation of what the "Cruncher" service returns.
 *
 * To avoid spreading 3rd-party models into the application, this class knows
 * how to translate itself to something that "Dummy" is aware of
 * (such as {@code SomeSummary}).
 */
@Value
public class SomeSummaryResponse {
  String name;
  String field3;
  BigDecimal total;

  public SomeSummary toSomeSummary() {
    return new SomeSummary(this.name, this.total);
  }
}
