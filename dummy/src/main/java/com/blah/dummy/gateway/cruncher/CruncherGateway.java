package com.blah.dummy.gateway.cruncher;

import com.blah.dummy.domain.GroupId;
import com.blah.dummy.domain.SomeSummary;
import com.blah.dummy.gateway.cruncher.model.SomeSummaryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * The {@code CruncherGateway} is responsible for communicating to the
 * "Cruncher" service.
 */
@Component
public class CruncherGateway {
  private final RestTemplate restTemplate;
  private final String baseUrl;

  @Autowired
  public CruncherGateway(
    RestTemplate restTemplate,
    @Value("${cruncher.url}") String cruncherUrl
  ) {
    this.restTemplate = restTemplate;
    this.baseUrl = cruncherUrl;
  }

  public SomeSummary someSummaryFor(GroupId groupId) {
    return restTemplate.getForObject(
      String.format("%s/groups/%s", this.baseUrl, groupId.value()),
      SomeSummaryResponse.class
    ).toSomeSummary();
  }
}
