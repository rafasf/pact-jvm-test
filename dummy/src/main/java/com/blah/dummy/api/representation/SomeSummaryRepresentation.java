package com.blah.dummy.api.representation;

import com.blah.dummy.domain.SomeSummary;
import lombok.Value;

import java.math.BigDecimal;

/**
 * The information being returned to consumers
 */
@Value
public class SomeSummaryRepresentation {
  String name;
  BigDecimal total;

  public static SomeSummaryRepresentation from(SomeSummary summary) {
    return new SomeSummaryRepresentation(summary.getName(), summary.getTotal());
  }
}
