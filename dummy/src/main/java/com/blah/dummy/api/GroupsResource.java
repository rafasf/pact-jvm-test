package com.blah.dummy.api;

import com.blah.dummy.api.representation.GroupRepresentation;
import com.blah.dummy.api.representation.SomeSummaryRepresentation;
import com.blah.dummy.gateway.cruncher.CruncherGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.blah.dummy.domain.GroupId.groupId;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * The {@code GroupsResource} exposes the group information
 *
 * Consumers should be able to:
 *  * View information for a specific group
 *
 * No domain model should be exposed to consumers, hence the creation of
 * "representations" for each of the domain models.
 */
@RestController
@RequestMapping("/groups")
public class GroupsResource {
  private final CruncherGateway cruncher;

  @Autowired
  public GroupsResource(CruncherGateway cruncher) {
    this.cruncher = cruncher;
  }

  @RequestMapping(value = "/{id}", produces = "application/json", method = GET)
  public ResponseEntity<GroupRepresentation> group(@PathVariable("id") String id) {
    return ok(new GroupRepresentation(
      "Super",
      SomeSummaryRepresentation.from(cruncher.someSummaryFor(groupId(id)))));
  }
}
