package com.blah.dummy.api.representation;

import lombok.Value;

/**
 * The information being returned to consumers
 */
@Value
public class GroupRepresentation {
  String name;
  SomeSummaryRepresentation summary;
}
